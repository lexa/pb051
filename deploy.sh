#!/bin/bash

#cp *.html /mnt/lexa/public_html/pb051/.
#cp -R images /mnt/lexa/public_html/pb051/.
#cp -R site_libs /mnt/lexa/public_html/pb051/.
#cp -R lesson1_files /mnt/lexa/public_html/pb051/.


scp *.html lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pb051/.
scp -r images lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pb051/.
scp -r site_libs lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pb051/.
scp -r lesson*_files lexa@helix.fi.muni.cz:/mnt/lexa/public_html/pb051/.
